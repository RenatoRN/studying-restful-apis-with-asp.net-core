using Microsoft.EntityFrameworkCore;
using Supermarket.API.Domain.Models;

namespace Supermarket.API.Persistence.Contexts
{
    public class AppDbContext : DbContext
    {
        public DbSet<Category> Categories { get; set; }
        public DbSet<Product> Products { get; set; }

        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            
            builder.Entity<Category>().ToTable("Categories");
            builder.Entity<Category>().HasKey(p => p.id);
            builder.Entity<Category>().Property(p => p.id).IsRequired().ValueGeneratedOnAdd();
            builder.Entity<Category>().Property(p => p.name).IsRequired().HasMaxLength(30);
            builder.Entity<Category>().HasMany(p => p.products).WithOne(p => p.category).HasForeignKey(p => p.categoryId);

            builder.Entity<Category>().HasData
            (
                new Category { id = 100, name = "Fruits and Vegetables" }, // id set manually due to in-memory provider
                new Category { id = 101, name = "Dairy" }
            );

            builder.Entity<Product>().ToTable("Products");
            builder.Entity<Product>().HasKey(p => p.id);
            builder.Entity<Product>().Property(p => p.id).IsRequired().ValueGeneratedOnAdd();
            builder.Entity<Product>().Property(p => p.name).IsRequired().HasMaxLength(50);
            builder.Entity<Product>().Property(p => p.quantityInPackage).IsRequired();
            builder.Entity<Product>().Property(p => p.unitOfMeasurement).IsRequired();
        }
    }
}